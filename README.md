#Follow-up sheet
Killian Pareilleux
Alan Guivarch

## Monday 27th january

Meeting with Jeff Knoepfli and Joris Bremond the project managers. 
present the three diferent subjects:
-open a connected door
-controle the building lighting
-monitoring the building temperature

## Monday 03rd february

Feasibility study on the different project (openHAB, KNX,...)
Meeting with Jeff and Joris at Moonshot Labs to visit the building and talk about the opening door project
(can call the door with a tel SIP to open it)

## Monday 10th february

Serverless architecture research.

## Monday 17th february

Conception of the project  final architecture   that meet the customer need.

Update of the detailed road map for the end of the project.

## Monday 02nd, tuesday 03rd march

Beginning of the front end development with angular and  automatic deployment implementation with serverless framwork.

### Monday 16th march, tuesday 17th march

Same as previous week.

### Monday 23th march, tuesday 24th march

Same as previous week.

### Monday 30th march, tuesday 31th march

Same as previous week.

###thursday 23th april

Meeting with Jeff and Joris. Presentation of the  frontend prototype. 
Front end correction after the meeting.

###Monday 27th april, tuesday 28th april

Finalization  of the frontend to complete the acceptance conditions, elaboration of the final report.
